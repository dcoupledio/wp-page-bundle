<?php namespace Decoupled\Page;

use Decoupled\Core\Bundle\BundleInterface;

class PageBundle implements BundleInterface{

    /**
     * @return     string  The id of the bundle.
     */

    public function getName()
    {
        return 'd.page';
    }

    /**
     * @return     string  The Bundle Dir
     */

    public function getDir()
    {
        return dirname(__FILE__);
    }

}

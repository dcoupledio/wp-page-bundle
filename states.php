<?php return function( Decoupled\Core\State\StateRouter $stateRouter ){

    $stateRouter('page.default')
        ->when('$app')
        ->uses(function(){

            $scope['baseLayout'] = @$scope['baseLayout'] ?: '@app/layout.html.twig';
        });


    $stateRouter('page.view')
        ->when('page')
        ->uses('Decoupled\\Page\\Controller\\PageController@view');
};
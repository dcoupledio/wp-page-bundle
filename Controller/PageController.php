<?php namespace Decoupled\Page\Controller;

class PageController{

    public function view( $scope, $out, $post )
    {
        $scope['page'] = $post;

        return $out('@d.page/single.html.twig', $scope);
    }
}